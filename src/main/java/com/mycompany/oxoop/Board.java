package com.mycompany.oxoop;


public class Board {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    public  boolean  Win = false;
    public  boolean  Draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }


    public Player getX() {
        return x;
    }


    public int getCount() {
        return count;
    }
    private void switchPlayer(){
        if(currentPlayer==o){
            currentPlayer=x;
        }else{
            currentPlayer=o;
        }
    }
    
    
    public  boolean  setRowCol(int row,int col){
        if(isWin() || isDraw()) return false;
        if(row>3 || col >3 || row < 1|| col < 1)return false;
        if(table[row-1][col-1]!='-')return false;
        table[row-1][col-1]=currentPlayer.getSymbol();
       if(checkWin(table, currentPlayer.getSymbol() , row, col)){
           if(this.currentPlayer == o){
               o.Win();
               x.Loss();
           }else{
               x.Win();
               o.Loss();
           }
           this.Win = true;
           return true;
       }
       if(checkDraw()){
           o.Draw();
           x.Draw();
           this.Draw = true;
           return  true;
       }
       count++;
        switchPlayer();
        return true;
    }

    public boolean isWin() {
        return Win;
    }

    public boolean isDraw() {
        return Draw;
    }
    
    public boolean checkDraw(){
        if(count==8){
            return true;
        }
        return false;
    }
    
    public boolean checkWin(char[][] table,char currentPlayer,int row,int col) {
        if (checkVertical(table,currentPlayer,col)) {
            return true;
        } else if (checkHorizontal(table,currentPlayer,row)) {
            return true;
        } else if (checkX(table,currentPlayer)) {
            return true;
        }
        return false;
    }

    public boolean checkVertical(char[][] table,char currentPlayer,int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX(char[][] table,char currentPlayer) {
        if (checkX1(table,currentPlayer)) {
            return true;
        } else if (checkX2(table,currentPlayer)) {
            return true;
        }
        return false;
    }

    public boolean checkHorizontal(char[][] table,char currentPlayer,int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX1(char[][] table,char currentPlayer) {// 11 22 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX2(char[][] table,char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

}
